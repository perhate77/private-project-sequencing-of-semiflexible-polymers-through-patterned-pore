**There are two serial codes of Langevin dynamics of homogeneous and hetereogeneous semiflexible polymer through patterned pore, written in C.**

Ref: Rajneesh Kumar, Abhishek Chaudhuri and Rajeev Kapri, Sequencing of semiflexible
polymers of varying bending rigidity using patterned pores, J. Chem. Phys. 148, 164901
(2018).
   
 	 

 *	 Compile code using: 
 *	 	 gcc filename.c -lm

      



 *	 Run :
 *		 nohup ./a.out &
 			
 ![tauDistTime](/uploads/34d1839f26c64aa08d6d305c557504a7/tauDistTime.png)

     
 Written By::<br/>
       **Rajneesh Kumar**
 *	Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 *	Bengaluru 560064, India.
 *	Email: rajneesh[at]jncasr.ac.in
 *	27 Dec, 2020
